class Titulaire {
	/**
	 *
	 * @param {string} name
	 * @param {string} firstname
	 */
	constructor(name, firstname) {
		this.name = name;
		this.firstname = firstname;
	}
	identite() {
		return this.firstname + ' ' + this.name;
	}
}
