class CompteEpargne extends Compte {
	/**
	 * Création du compte épargne
	 * @param {Titulaire} titulaire
	 * @param {number} montant
	 * @param {number} taux
	 * @param {number} rythme
	 */
	constructor(titulaire, montant = 50, taux = 0.005, rythme = 1000) {
		super(titulaire, montant);
		this.taux = taux;
		this.rythme = rythme;

		setInterval(() => {
			this.solde *= 1 + this.taux;
		}, this.rythme);
	}
}
