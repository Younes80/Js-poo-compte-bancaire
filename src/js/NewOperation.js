class NewOperation extends Compte {
	/**
	 *
	 * @param {Titulaire} titulaire
	 * @param {string} operation
	 * @param {number} montant
	 */
	constructor(titulaire, operation, montant) {
		super(titulaire);
		this.operation = operation;
		this.montant = montant;
	}
}
