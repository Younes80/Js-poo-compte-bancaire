class Compte {
	/**
	 * Création du compte
	 * @param {Titulaire}
	 * @param {number}
	 */
	constructor(titulaire, montant = 0) {
		this.titulaire = titulaire;
		this.solde = montant;
	}

	/**
	 *
	 * @param {number} montant
	 */
	crediter(montant) {
		this.solde += montant;
	}

	/**
	 *
	 * @param {number} montant
	 */
	debiter(montant) {
		this.solde -= montant;
	}

	/**
	 *
	 * @returns Solde
	 */
	afficherSolde() {
		return parseInt(this.solde);
	}
}
