$(document).ready(function () {
	let dateNow = new Date(Date.now());
	const options = {
		year: 'numeric',
		month: 'numeric',
		day: 'numeric',
	};
	date = dateNow.toLocaleDateString('fr-FR', options);

	$('#add-credit').submit(function (e) {
		// e.preventDefault();
		$('#credit-titulaire-name').val();
		data = $(this).serializeArray();
		console.log(data);
		let dataIdCourant = $('#compte-courant').attr('data-compte-courant');
		let dataIdEpargne = $('#compte-epargne').attr('data-compte-epargne');

		let titulaire = new Titulaire(data[3].value, data[4].value);
		let compte1 = new Compte(titulaire, parseInt(data[5].value));
		let CompteEpargne1 = new CompteEpargne(titulaire, parseInt(data[7].value));
		newOperation = new NewOperation(
			data[6].value,
			data[2].name,
			parseInt(data[2].value)
		);
		console.log(compte1);
		console.log(CompteEpargne1);
		console.log(titulaire);
		console.log(newOperation);
		if (data[0].value == '1') {
			if (data[1].value === 'Crédit') {
				compte1.crediter(parseInt(data[2].value));
				$('#operations').prepend(`<tr>
				<th class="date-operation" scope="row">${date}</th>
				<td class="description-operation">${data[1].value}</td>
				<td class="montant-operation">${parseInt(data[2].value)}</td>
			</tr>`);
			} else if (data[1].value === 'Débit') {
				compte1.debiter(parseInt(data[2].value));
				$('#operations').prepend(`<tr>
				<th class="date-operation" scope="row">${date}</th>
				<td class="description-operation">${data[1].value}</td>
				<td class="montant-operation">${parseInt(data[2].value)}</td>
			</tr>`);
			}
			$('#titulaire-solde-courant').html(compte1.afficherSolde() + ' €');
			let data5 = compte1.afficherSolde();
			console.log(data5);
			console.log(compte1.afficherSolde());

			$.get('../server/model/insert_compte_courant.php', {
				data1: data[2].value,
				data2: data[1].value,
				data3: data[6].value,
				data4: dataIdCourant,
			});
			$.get('../server/model/update_solde_courant.php', {
				id: data[6].value,
				data5: data5,
			});
		} else if (data[0].value == '2') {
			if (data[1].value === 'Crédit') {
				CompteEpargne1.crediter(parseInt(data[2].value));
				$('#operations-epargne').prepend(`<tr>
				<th class="date-operation" scope="row">${date}</th>
				<td class="description-operation">${data[1].value}</td>
				<td class="montant-operation">${parseInt(data[2].value)}</td>
			</tr>`);
			} else if (data[1].value === 'Débit') {
				CompteEpargne1.debiter(parseInt(data[2].value));
				$('#operations-epargne').prepend(`<tr>
				<th class="date-operation" scope="row">${date}</th>
				<td class="description-operation">${data[1].value}</td>
				<td class="montant-operation">${parseInt(data[2].value)}</td>
			</tr>`);
			}
			$('#titulaire-solde-epargne').html(CompteEpargne1.afficherSolde() + ' €');
			console.log(CompteEpargne1.afficherSolde());
			let data7 = CompteEpargne1.afficherSolde();
			console.log(data7);
			console.log(CompteEpargne1.afficherSolde());
			$.get('../server/model/insert_compte_epargne.php', {
				data1: data[2].value,
				data2: data[1].value,
				data3: data[6].value,
				data4: dataIdEpargne,
			});
			$.get('../server/model/update_solde_epargne.php', {
				id: data[6].value,
				data5: data7,
			});
		}
		$('#add-credit')
			.find(' #crediter, select')
			.val('')
			.end()
			.find(':checked')
			.prop('checked', false);
	});
});
