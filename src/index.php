<?php
session_start();

require_once '../server/controllers/controller.php';
require_once '../server/model/model.php';
require_once '../server/model/read_users.php';

if(isset($_GET['p'])){
    $p = $_GET['p'];
} else {
    $p = 'home';
}

ob_start();

if($p ==='home') {
    require 'views/home.php';
} else if($p === 'login') {
    loginController();
} else if($p === 'register') {
    registerController();
} else if($p === 'comptes') {
    require 'views/comptes.php';
} else if($p == 'disconnect'){
    disconnectController();
}else {
    require 'views/home.php';
}
  
$content = ob_get_clean();


require 'views/templates/default.php';