<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-6 mb-5">
            <form action="<?php echo $_SERVER['PHP_SELF'].'?p=register'; ?>" method="POST" class="border rounded shadow p-5 bg-light">
                <h2 class="mb-5 text-center">S'inscrire</h2>
                <input type="text" name="username_register" id="username_register" placeholder="Votre nom" class="form-control mb-3" required>
                <input type="text" name="firstname_register" id="firstname_register" placeholder="Votre prénom" class="form-control mb-3" required>
                <input type="number" name="phone_register" id="phone_register" placeholder="Votre numéro de téléphone" class="form-control mb-3" required>
                <input type="email" name="email_register" id="email_register" placeholder="Votre e-mail" class="form-control mb-3" required>
                <input type="password" name="password_register" id="password_register" placeholder="Votre mot de passe" class="form-control mb-3" required>
                <div class="d-flex justify-content-between">
                    <span class="psw"><a href="index.php?p=login">Déjà un compte ?</a></span>
                    <input type="submit" name="form_register" class="btn btn-secondary rounded-pill" value="Valider">
                </div>
            </form>
        </div>
    </div>
</div>