
<div class="container home d-flex flex-column align-items-center justify-content-center bg-light mt-5 py-5 w-sm-50 rounded-3">
    <div class="row mb-5">
        <div class="col-md-12">
            <h1>Ma banque personnelle</h1>
            <p>Faites le suivi de vos dépenses et de vos revenus </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <a class="btn btn-secondary mx-3 rounded-pill" href="index.php?p=register">S'inscrire</a>
        <a class="btn btn-secondary mx-3 rounded-pill" href="index.php?p=login">Se connecter</a>
        </div>
    </div>
</div>

