<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Document</title>
        <script src="https://kit.fontawesome.com/65730c3f1e.js" crossorigin="anonymous"></script>
		<link
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
			rel="stylesheet"
		/>
		<link rel="stylesheet" href="assets/css/style.css" />
	</head>
	<body>
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top align-items-center">
                <div class="container-fluid align-items-center">
                    <a class="navbar-brand" href="index.php?p=home">My Bank</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0 align-items-center">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php?p=home">Home</a>
                            </li>
                            <?php if(isset($_SESSION['email'])): ?>
                                <li class="nav-item">
                                <a class="nav-link <?php echo ($p == 'member') ? 'active' : '' ?>" href="index.php?p=member">Member</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($p == 'comptes') ? 'active' : '' ?>" href="index.php?p=comptes">Comptes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?p=disconnect"><i class="fas fa-2x fa-power-off text-danger"></i></a>
                            </li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <main class="py-5">

            <?= $content; ?>


        </main><!-- /.container -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"></script>
		<script src="js/Titulaire.js"></script>
		<script src="js/Compte.js"></script>
		<script src="js/NewOperation.js"></script>
		<script src="js/CompteEpargne.js"></script>
		<script src="main.js"></script>
	</body>
</html>
