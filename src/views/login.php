
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-6 mb-5">
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>?p=login" method="POST" class="border rounded shadow p-5 bg-light">
                <h2 class="mb-5 text-center">Se connecter</h2>
                <input type="email" name="email_login" id="email_login" placeholder="Votre e-mail" class="form-control mb-3" required>
                <input type="password" name="password_login" id="password_login" placeholder="Votre mot de passe" class="form-control mb-3" required>
                <div class="d-flex justify-content-between align-items-center"> 
                    <div class="d-flex flex-column">
                        <span class="psw p-0 m-0 d-block"><a class="p-0 m-0" href="index.php?p=register">Créer un compte ?</a></span>
                        <span class="psw p-0 m-0 d-block"><a class="p-0 m-0" href="index.php?p=forgot-pw">Mot de passe oublié ?</a></span>
                    </div>
                    <input type="submit" name="form_login" class="btn btn-secondary rounded-pill" value="Valider">
                </div>
            </form>
        </div>
    </div>
</div>