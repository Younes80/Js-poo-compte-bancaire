<?php
   if(isLogged() == 0){
      header("Location:index.php?page=home");
   }

   $getUser = readUser($_SESSION['id']);
   $getCourantUser = readCourantUser($_SESSION['id']);
   $getEpargneUser = readEpargneUser($_SESSION['id']);

?>
<div class="container pt-5">
	<div class="row mb-5 justify-content-around align-items-center">
		<div class="col-md-5">
			<div class="card border-0">
				<div class="card-body">
					<?php   foreach ($getUser as  $value):?>
					<h2 class="border-bottom" id="titulaire-name" data-id="<?= $value['id']?>"><?= $value['first_name'] . ' ' . $value['name'] ?></h2>
					<div class="mt-3">
						<span class="">Compte courant : </span>
						<span id="titulaire-solde-courant" class="fw-bold"><?= $value['solde'] ?> €</span>
					</div>
					<div class="mt-3">
						<span class="">Compte épargne : </span>
						<span id="titulaire-solde-epargne" class="fw-bold"><?= $value['solde_epargne'] ?> €</span>
					</div>
					<?php endforeach?>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="card border-0 p-3">
                <?php
                foreach (readUser($_SESSION['id']) as  $value): ?>
				<form id="add-credit" method="post">
					<h2 class="border-bottom">Dépôt/Retrait</h2>
                    <input type="radio" data-compte-courant="Courant" name="compte" id="compte-courant" value="1" checked>
                    <label for="compte-courant">Compte courant</label>
                    <input type="radio" data-compte-epargne="Epargne" name="compte" id="compte-epargne" value="2">
                    <label for="compte-epargne">Compte épargne</label>
					<select
						class="form-select mt-3"
						name="crediter-debiter"
						id="crediter-debiter"
						required
					>
						<option value="" selected disabled hidden>
							Dépôt/Retrait
						</option>
						<option value="Crédit">Dépôt</option>
						<option value="Débit">Retrait</option>
					</select>
					<input
						class="form-control my-3"
						type="number"
						name="credit"
						id="crediter"
						step=".01"
						min="0"
						required
					/>
					<input
						type="text"
						name="credit-titulaire-name"
						id="credit-titulaire-name"
						value="<?= $value['name']?>"
						hidden
					/>
                    <input
						type="text"
						name="credit-titulaire-firstname"
						id="credit-titulaire-firstname"
						value="<?= $value['first_name']?>"
						hidden
					/>
                    <input
						type="text"
						name="credit-titulaire-solde"
						id="credit-titulaire-solde"
						value="<?= $value['solde'] ?>"
						hidden
					/>
                    <input
						type="text"
						name="credit-titulaire-id"
						id="credit-titulaire-id"
						value="<?= $value['id_compte'] ?>"
						hidden
					/>
					<input
						type="text"
						name="credit-titulaire-solde-epargne"
						id="credit-titulaire-solde-epargne"
						value="<?= $value['solde_epargne'] ?>"
						hidden
					/>
					<input
						class="btn btn-secondary"
						type="submit"
						value="Valider le montant"
					/>
				</form>
                <?php endforeach ?>
			</div>
		</div>
    </div>
	<div class="row">
		<div class="col-md-6">
			<div class="card border-0 p-3">
				<h2 class="border-bottom">Compte courant</h2>
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Date</th>
							<th scope="col">Opération</th>
							<th scope="col">Montant</th>
						</tr>
					</thead>
					<tbody id="operations">
                        <?php foreach ($getCourantUser as $value): ?>
							<?php
								
								$date = $value['date_time'];
								$date = explode('-', $value['date_time']);
								$date = $date[2].'/'.$date[1]."/".$date[0];
							?>
                        <tr>
                            <th class="date-operation" scope="row"><?= $date?></th>
                            <td class="description-operation"><?= $value['operation']?></td>
                            <td class="montant-operation"><?= $value['montant']?></td>
                        </tr>
                           <?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card border-0 p-3">
				<h2 class="border-bottom">Compte épargne</h2>
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Date</th>
							<th scope="col">Opération</th>
							<th scope="col">Montant</th>
						</tr>
					</thead>
					<tbody id="operations-epargne">
						<?php foreach ($getEpargneUser as  $value): ?>
							<?php
								
								$date = $value['date_time'];
								$date = explode('-', $value['date_time']);
								$date = $date[2].'/'.$date[1]."/".$date[0];
							?>
							<tr>
								<th class="date-operation" scope="row"><?= $date?></th>
								<td class="description-operation"><?= $value['operation']?></td>
								<td class="montant-operation"><?= $value['montant']?></td>
							</tr>
                        <?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

