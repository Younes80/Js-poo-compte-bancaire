<?php

require_once 'connect.php';


function readCourantUser($id) {
    try {
    $db = dbConnect();
        $request = "SELECT users.*, comptes.*, operations.* FROM `users`
            INNER JOIN `comptes` ON users.id = comptes.id_compte
            INNER JOIN `operations` ON users.id = operations.id_user
            WHERE users.id = :id
            ORDER BY operations.id_operation DESC";
        $exec = $db->prepare($request);
        $exec->bindValue(":id", $id);
        $exec->execute();
        // header('Access-Control-Allow-Origin: *');
        // header('Content-type: application/json');
        $data = $exec->fetchall(PDO::FETCH_ASSOC);
        // $data = $exec->fetchall();
        // echo json_encode($data);
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        return $data;
        // exit();
    } catch(PDOException $e) {
            echo $e->getMessage();
    }
}

function readEpargneUser($id) {
    try {
    $db = dbConnect();
        $request = "SELECT users.*, comptes_epargne.*, operations_epargne.* FROM `users`
            INNER JOIN `comptes_epargne` ON users.id = comptes_epargne.id_epargne
            INNER JOIN `operations_epargne` ON users.id = operations_epargne.id_user
            WHERE users.id = :id
            ORDER BY operations_epargne.id_operation DESC";
        $exec = $db->prepare($request);
        $exec->bindValue(":id", $id);
        $exec->execute();
        // header('Access-Control-Allow-Origin: *');
        // header('Content-type: application/json');
        $data = $exec->fetchall(PDO::FETCH_ASSOC);
        // $data = $exec->fetchall();
        // echo json_encode($data);
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        return $data;
        // exit();
    } catch(PDOException $e) {
            echo $e->getMessage();
    }
}

function readUser($id) {
    try {
    $db = dbConnect();
        $request = "SELECT users.*, comptes.*, comptes_epargne.*  FROM `users`
            INNER JOIN `comptes` ON users.id = comptes.id_compte
            INNER JOIN `comptes_epargne` ON users.id = comptes_epargne.id_epargne
            WHERE users.id = :id";
        $exec = $db->prepare($request);
        $exec->bindValue(":id", $id);
        $exec->execute();
        $data = $exec->fetchall(PDO::FETCH_ASSOC);
        return $data;
    } catch(PDOException $e) {
            echo $e->getMessage();
    }
}