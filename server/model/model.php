<?php
require 'connect.php';


function registerForm($username, $firstname, $email ,$password,$phone){
    $db = dbConnect();

    /* Vérifions qu'aucun utilisateur n'existe avec le même email */
    $sql_verif_2 = "SELECT COUNT(email) AS num FROM users WHERE email = :email";
    $stmt = $db->prepare($sql_verif_2);
    $stmt-> bindValue(':email', $email);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if( $row['num'] > 0 ){
        header("Location:index.php?error=register4");
        die();
    }

    $password_hash_options = [
        'cost' => 12,
    ];
    $password_hash = password_hash($password, PASSWORD_BCRYPT, $password_hash_options);


    $sql_insert = "INSERT INTO users (
            name,
            first_name,
            email,
            password,
            phone
        ) VALUES (
            :name,
            :first_name,
            :email,
            :password,
            :phone
    )";

    $stmt = $db->prepare($sql_insert);

    $stmt->bindValue(':name', $username);
    $stmt->bindValue(':first_name', $firstname);
    $stmt->bindValue(':email', $email);
    $stmt->bindValue(':password', $password_hash);
    $stmt->bindValue(':phone', $phone);

    $sql_insert1 = "INSERT INTO comptes (
        compte,
        id_user,
        solde
        ) VALUES (
        :compte,
        :id_user,
        :solde
    )";

    $stmt1 = $db->prepare($sql_insert1);

    $stmt1->bindValue(':compte', 'Courant');
    $stmt1->bindValue(':id_user', $email);
    $stmt1->bindValue(':solde', 50);

    $sql_insert2 = "INSERT INTO comptes_epargne (
        epargne,
        id_user,
        solde_epargne,
        taux,
        rythme
        ) VALUES (
        :epargne,
        :id_user,
        :solde,
        :taux,
        :rythme
    )";

    $stmt2 = $db->prepare($sql_insert2);

    $stmt2->bindValue(':epargne', 'Epargne');
    $stmt2->bindValue(':id_user', $email);
    $stmt2->bindValue(':solde', 50);
    $stmt2->bindValue(':taux', 0);
    $stmt2->bindValue(':rythme', 0);

    $result_insert = $stmt->execute();
    $result_insert1 = $stmt1->execute();
    $result_insert2 = $stmt2->execute();

    return $result_insert; $result_insert1; $result_insert2;

}
// if(isset($_GET['token]) && $_GET['token] !== "")
// function token($token, $emailForgot) {
//     $db = dbConnect();
//     $sql = "UPDATE users SET token = ? WHERE email = ?";
//     $stmt = $db->prepare($sql);
//     $stmt->execute([$token, $emailForgot]);
// }


function loginUser($email, $password){
    $db = dbConnect();
    $request = "SELECT * FROM users WHERE email = :email LIMIT 1";
    $exec = $db->prepare($request);
    $exec->bindValue(":email", $email);
    $exec->execute();
    // Est ce qu'il existe un résultat avec ce username dans la BDD
    $result_login = $exec->fetchAll();
    // print_r($password);
    // print_r($result_login[0]['password']);
    // exit();
    $password_check = password_verify($password, $result_login[0]['password']);
    if($password_check == $result_login[0]['password']){
            $_SESSION['email'] = $email;
            $_SESSION['id'] = $result_login[0]['id'];
            $_SESSION['name'] = $result_login[0]['name'];
            $_SESSION['first_name'] = $result_login[0]['first_name'];
            return true;
    }else{ 
        return false;
    }
}

function isLogged(){
    if(isset($_SESSION['email'])){
        $logged = 1;
    }else{
        $logged = 0;
    }
    return $logged;
    $db = dbConnect();
    $sql = "UPDATE users SET is_logged = ? WHERE username = ?";
    $stmt = $db->prepare($sql);
    // $stmt-> bindValue(':is_logged', $logged);
    $stmt->execute([$logged]);
    // echo '<pre>';
    // print_r($stmt);
    // echo '</pre>';
    // exit();
}

// function get_members(){
//     $db = dbConnect();
//     $req = $db->query("SELECT * FROM users");
//     $results = array();
//     while($rows = $req->fetchObject()){
//         $results[] = $rows;
//     }
//     return $results;
// }


// function user_exist(){
//     $db = dbConnect();
//     $e = array('user' => $_GET['user'], 'session'=>$_SESSION['username']);
//     $sql = "SELECT * FROM users WHERE username =:user AND username != :session";
//     $req = $db->prepare($sql);
//     $req->execute($e);
//     $exist = $req->rowCount($sql);
//     return $exist;
// }

// function get_user(){
//     $db = dbConnect();
//     $req = $db->query("SELECT * FROM users WHERE username = '{$_SESSION['receiver']}'");
//     $user = array();
//     while($row = $req->fetchObject()){
//         $user[] = $row;
//     }
//     return $user;
// }


