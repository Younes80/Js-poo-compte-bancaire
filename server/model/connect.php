<?php

define('MYSQL_HOST', 'localhost');
define('MYSQL_DATABASE', 'my_bank');
define('MYSQL_USER', 'root');
define('MYSQL_PASSWORD', '');
// define('MYSQL_HOST', 'cl1-sql11');
// define('MYSQL_DATABASE', 'p4704_9');
// define('MYSQL_USER', 'p4704_9');
// define('MYSQL_PASSWORD', 'vWAYAylvTS46');

function dbConnect() {
    try {
        $db = new PDO("mysql:host=".MYSQL_HOST.";dbname=".MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD);
        // set the PDO error mode to exception
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}