<?php

require_once 'connect.php';



    try{
        $db = dbConnect();
        $idUser = $_GET['data3'];
        $idCompte = $_GET['data4'];
        $montant = $_GET['data1'];
        $operation = $_GET['data2'];
        $today = date("Y-m-d");
    
        $request = "INSERT INTO `operations`(
            `id_user`,
            `id_compte`,
            `operation`,
            `montant`,
            `date_time`
            ) VALUES (
            :id_user,
            :id_compte,
            :operation,
            :montant,
            :date_time
            )";
      
        $exec = $db->prepare($request);
        $exec->bindValue(':id_user', $idUser );
        $exec->bindValue(':id_compte', $idCompte );
        $exec->bindValue(':operation', $operation );
        $exec->bindValue(':montant', $montant );
        $exec->bindValue(':date_time', $today );
        $exec->execute();


        

        return true;
    }catch(PDOException $e) {
        echo $e->getMessage();
    }

