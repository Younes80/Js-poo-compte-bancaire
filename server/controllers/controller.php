<?php 


function registerController(){
    
    if( isset($_POST['form_register']) ){
        $username = $_POST['username_register'];
        $password = $_POST['password_register'];
        $email = $_POST['email_register'];
        $firstname = $_POST['firstname_register'];
        $phone = $_POST['phone_register'];
        
        $result = registerForm($username, $firstname, $email ,$password,$phone);
        // var_dump($result);
        // exit();
        if($result == true){
            $result = loginUser($email, $password);
                include 'views/comptes.php';
        } else {
            include 'views/register.php';
        }

    }else {
        if(isset($_SESSION['email']) && $_SESSION['email'] !== null){
            header('Location: index.php?p=comptes');
                
                exit();
        }else{
            include 'views/register.php';      
        } 
    }
}

function loginController(){

    if(isset($_POST['form_login'])){

        $email = $_POST['email_login'];
        $password = $_POST['password_login'];
    
        $result_login = loginUser($email, $password);
        if($result_login == true){
            header('Location: index.php?p=comptes');
            
        }else{
            include 'views/login.php';
            echo 'error ici';
        }
    }else{
        if(isset($_SESSION['email']) && $_SESSION['email'] !== null){
            // include 'views/listByUser.php';
            header('Location: index.php?p=comptes');
        }else{
            include 'views/login.php';
        
        }
    }   
}

// function forgotPassword() {

//     if (isset($_POST['email_forgot_wp']))
//     {
//         $emailForgot = $_POST['email_forgot_wp'];
//         $token = uniqid();
//         $url = "http://localhost/projets/formationCDA/todoList_php/views/token/index.php?token=$token";
//         $message = "Bonjour, voici votre lien pour réinitialiser votre mot de passe : $url";
//         $headers =  'MIME-Version: 1.0' . "\r\n"; 
//         $headers .= 'From: Younes <younes.haidri@gmail.com>' . "\r\n"; 
//         $headers .= 'Content-Type: text/plain; charset="utf-8"'."";

//         if (mail($emailForgot, 'Mot de passe oublié', $message, $headers))
//         {
//             token($token, $emailForgot);
//             header("Location:index.php?p=login");
//         } else {
//             echo "Une erreur est survenue...";
//         }
//     }
//     require 'views/forgot_pw.php';
// }


function disconnectController(){
    session_destroy();
    session_unset();
    header('Location: index.php');
}